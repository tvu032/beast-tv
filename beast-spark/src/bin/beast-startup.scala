println(
  """
    |                 ___   ___       ___ ___
    |  Empowered by:   __) |__   /\  /__   |
    |                 |__) |___ /__\ ___/  |    version ${version}
    |  Visit https://bitbucket.org/eldawy/beast/wiki for more details
    |""")
import edu.ucr.cs.bdlab.beast._
org.apache.spark.beast.CRSServer.startServer(sc)
org.apache.spark.beast.SparkSQLRegistration.registerUDT
org.apache.spark.beast.SparkSQLRegistration.registerUDF(spark)
